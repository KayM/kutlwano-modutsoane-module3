// ignore: unused_import
import 'package:flutter/material.dart';
import 'package:module3_app/pure_fumes.dart';

class DiffuserBlends extends StatelessWidget {
  const DiffuserBlends({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Diffuser Blends',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Aromatic Diffuser Blends'),
        ),
        body: const Center(
          child: Text('Diffuser Blends'),
        ),
        floatingActionButton: FloatingActionButton(
          tooltip: 'Next Page',
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const PureFumes()));
          },
        ),
      ),
    );
  }
}
