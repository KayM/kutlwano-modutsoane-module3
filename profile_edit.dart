import 'package:flutter/material.dart';

class ProfileEditRoute extends StatelessWidget {
  const ProfileEditRoute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Chakra Charm',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightGreen,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {},
          ),
          title: const Text('Edit Profile'),
        ),
        body: const Center(
          child: Text('Profile Edit'),
        ),
      ),
    );
  }
}
