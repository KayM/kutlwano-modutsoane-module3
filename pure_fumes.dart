import 'package:flutter/material.dart';
import 'package:module3_app/profile_edit.dart';

class PureFumes extends StatelessWidget {
  const PureFumes({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Aromatic PureFumes',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('PureFumes'),
        ),
        body: const Center(
          child: Text('Perfume Blends'),
        ),
        floatingActionButton: FloatingActionButton(
          tooltip: 'Next Page',
          child: const Icon(Icons.navigate_next),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const ProfileEditRoute()));
          },
        ),
      ),
    );
  }
}
