import 'package:flutter/material.dart';
import 'package:module3_app/pure_fumes.dart';
import 'package:module3_app/diffuser_blends.dart';
import 'package:module3_app/my_theme.dart';

void main() => runApp(const Dashboard());

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  static const String title = 'Dashboard';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: title,
      theme: MyTheme.lightTheme,
      home: Scaffold(
        appBar: AppBar(
          title: const Center(child: Text(title)),
        ),
        body: const MyStatefulWidget(),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  get floatingActionButton => null;

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style =
        ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));

    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const DiffuserBlends()),
              );
            },
            child: const Text('Diffuser Blends'),
          ),
          const SizedBox(height: 30),
          ElevatedButton(
            style: style,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const PureFumes()),
              );
            },
            child: const Text('PureFumes'),
          ),
        ],
      ),
    );
  }
}
