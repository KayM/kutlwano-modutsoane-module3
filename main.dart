import 'package:flutter/material.dart';
import 'package:module3_app/log_in_route.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  static const String title = 'Login Page';

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: title,
      home: LogInRoute(),
    );
  }
}
